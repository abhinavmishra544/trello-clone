import React, { Component } from 'react'
const {getChecklists,addChecklist,deleteChecklist}=require('./../api/apifunctions')

export class Checklists extends Component {
    state={checklists:null,
    addedchecklist:''}
    onSubmit = (e) => {
	    e.preventDefault();
	    addChecklist(this.state.addedchecklist,this.props.location.state.cardid).then((addedList)=>{
           let checklist=this.state.checklists;
           checklist.push(addedList)
           this.setState({checklists:checklist})
        this.setState({ addedchecklist: '' })
        console.log(this.state.checklists)
	    })
       }
    onChange = (e) => this.setState({addedchecklist: e.target.value });
    componentDidMount()
    {
        getChecklists(this.props.location.state.cardid).then((data)=>{this.setState({checklists:data})
    })
    }
    render() {
        return (
            <div className="checklists">
                <header className="checklists-header"><h3>Checklists</h3>
<button type="button" className="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
  +
</button>
<div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
    <div className="modal-header">
           
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div><form onSubmit={this.onSubmit}>
       <div className="modal-body"><div className="input-group input-group-sm mb-3">
      <div className="input-group-prepend">
    <span className="input-group-text" id="inputGroup-sizing-sm">Checklist Name</span>
  </div>
      <input type="text" className="form-control" value={this.state.addedchecklist} onChange={this.onChange} aria-label="Small" aria-describedby="inputGroup-sizing-sm"/>
      </div></div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" className="btn btn-success">Add</button>
      </div>
  </form> </div>
  </div>
</div>
                </header>
             {this.state.checklists?(this.state.checklists.map((checklist)=>Checklist(checklist))):<h1>Loading</h1>}
             
            </div>
        )
    }
}
 function Checklist(props) {
    return<React.Fragment>{console.log(props)}
    <div className="checklist-item">
    <section className="checklists-item">{props.name}</section>
    <button type="button" onClick={()=>onDelete(props.id)} class="btn btn-danger btn-sm">&times;</button>
    </div><br/>
    </React.Fragment>
}
function onDelete(checklistId){
    deleteChecklist(checklistId)
 }


export default Checklists
