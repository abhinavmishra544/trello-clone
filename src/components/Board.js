import React, { Component } from 'react';
	import { Link } from 'react-router-dom';

export class Board extends Component {
	render() {
		return (
			<div class="card-body">
				<h5 class="card-title">
					<Link className="board_item" to={{ pathname: `/:${this.props.id}`, state: { boardid: this.props.id } }}>
						{this.props.name}
					</Link>
				</h5>
				{'   '}
			</div>
		);
	}
}

export default Board;
