import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export class Card extends Component {
    render() {
        return (
            <div class="card-item"> 
            <Link style={{textDecoration:'none'}} to={{ pathname: `/card/:${this.props.id}`, state: { cardid: this.props.id } }}>
            <section className="item">{this.props.name}</section>
            </Link>
            </div>
        )
    }
}

export default Card
