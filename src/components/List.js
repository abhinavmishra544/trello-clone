import React, { Component } from 'react';
import Card from './card';
const apiUtilities = require('./../api/apifunctions');

export class List extends Component {
	state = {
		cards: null,
		formcontrol: {
			display: 'none',
			width:'96%',
		},
		addedCardname: '',
	};
	componentDidMount() {
		apiUtilities
			.getCards(this.props.id)
			.then(data => { 
				return this.setState({ cards: data });
			})
			.catch(err => {
				console.log(err);
				return err;
			});
	}
	onClick = () => {
		this.setState({ formcontrol: { display: 'block' } });
	};
	onChange = (e) => this.setState({ addedCardname: e.target.value });
	onSubmit = (e) => {console.log(this.props)
        console.log(this.state.addedCardname+"------"+this.props.id)

		e.preventDefault();
		apiUtilities.addCard(this.state.addedCardname,this.props.id).then(addedCard => {
			let card= this.state.cards;
			card.push(addedCard);
			this.setState({ cards: card });
            this.setState({ addedCardname: '' });
            this.setState({ formcontrol: { display: 'none' } });
		});
	};
	render() {
       
		return (<div style={{margin:'1%'}}>
			<div className="List">
				<div class="list-header">
					<span class="list-name">{this.props.name}</span>
				</div>
				{this.state.cards ? (
					this.state.cards.map(card => {
						return <Card {...card} />;
					})
				) : (
					<h1>loading</h1>
				)}
				<button
					type="button"
					onClick={this.onClick}
					className="btn btn-secondary sm addCardbutton"
					data-toggle="modal"
					data-target="#exampleModal"
				>
					+ {'    '}Add card
				</button>
			</div>
			<form onSubmit={this.onSubmit}>
			<input
				style={this.state.formcontrol}
				onChange={this.onChange}
				value={this.state.addedCardname}
				type="text"
				className="form-control"
				aria-label="Sizing example input"
				aria-describedby="inputGroup-sizing-sm"
			/>
			<button
				style={this.state.formcontrol}
				type="submit"
				class="btn btn-success btn-sm">
				Save
			</button>
		</form>{' '}</div>
		);
	}
}
export default List;
