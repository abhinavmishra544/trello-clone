import React, { Component } from 'react';
import Board from './Board'
// import { Link } from 'react-router-dom';
const  apiUtilities=require('./../api/apifunctions') ;
export class Boards extends Component {
    state={boards:null}
    componentDidMount()
    {
        apiUtilities.getBoards().then((data)=>this.setState({boards:data}))
    }
	render() {
		return (
			<div className="container">
            <h1>
                {this.state.boards?(this.state.boards.map((board)=><Board className="board" {...board}/>)):<h1>Loading</h1>}
               </h1> 
                </div>
		);
	}   
}

export default Boards;
