import React, { Component } from 'react';
import List from './List';
const {getLists,addList} = require('./../api/apifunctions');

export class Lists extends Component {
	state = {
		lists: null,
		addedlistname: '',
	};
	onSubmit = (e) => {console.log(this.state.addedlistname)
	    e.preventDefault();
	    addList(this.state.addedlistname,this.props.location.state.boardid).then((addedList)=>{
           let list=this.state.lists;
           list.push(addedList)
           this.setState({lists:list,})
        this.setState({ addedlistname: '' })
	    })
       }
	onChange = (e) => this.setState({addedlistname: e.target.value });
	
	componentDidMount() {
	  getLists(this.props.location.state.boardid)
			.then(res => {
				
				return res;
			})
            .then(data =>this.setState({ lists: data }))
			.catch(err => console.log(err));
    }
  
	render() {
		return (
			<div className="lists-page">
				<div className="Lists">
					{this.state.lists ? (
						this.state.lists.map(list => <List className="list" {...list} />)
					) : (
						<title>Loading</title>
					)}
				<div>
				<form className="list-form" onSubmit={this.onSubmit}>
                <span class="badge badge-secondary" style={{textAlign:'center',display:'flex',flexDirection:'column',justifyContent:'center'}}>Add List</span> 
				<input
				value={this.state.addedlistname} 
				onChange={this.onChange}
				aria-label="Sizing example input"
				aria-describedby="inputGroup-sizing-sm"
			/>
					{' '}   
					<button type="submit"  class="btn btn-light">+</button>
				</form>
                </div>
                </div>
			</div>
		);
	}
}

export default Lists;
