import React from 'react';
import './App.css';
import {BrowserRouter as Router ,Route} from 'react-router-dom';
import Boards from './components/boards'
import Lists from './components/lists'
import Checklists from './components/Checklists';

function App() {
  return (
    <Router>
      <Header/>
      <Route path='/'  exact component={Boards} />
      <Route path='/:boardid' exact  component={Lists} />
      <Route path='/card/:cardid' exact component={Checklists}/>
    </Router>
  );
}

 function Header() {
 return <header class="title">Trello</header>
}

export default App;
