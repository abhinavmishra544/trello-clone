 function getBoards()
{
 return fetch('https://api.trello.com/1/members/abhinavmishra47/boards?memberships=none&key=7bcd9670004fd4a9b89efab01e92d773&token=14dc1d91d79d00ea6b7da57a9e85cb2ef8a63cfc1f7f2b23ec402416e71d2c70')
.then((res)=>(res.json()))
.catch((error)=>error)
}
function getLists(boardId)
{
 return fetch("https://api.trello.com/1/boards/"+boardId+"/lists?cards=none&card_fields=all&filter=open&fields=all&key=7bcd9670004fd4a9b89efab01e92d773&token=14dc1d91d79d00ea6b7da57a9e85cb2ef8a63cfc1f7f2b23ec402416e71d2c70")
.then((res)=>(res.json()))
.catch((error)=>console.log("hi"))
}
function getCards(cardId)
{
 return fetch("https://api.trello.com/1/lists/"+cardId+"/cards?fields=name%2Cclosed%2CidBoard%2Cpos&key=7bcd9670004fd4a9b89efab01e92d773&token=14dc1d91d79d00ea6b7da57a9e85cb2ef8a63cfc1f7f2b23ec402416e71d2c70")
.then((res)=>(res.json()))
.catch((error)=>console.log("hi"))
}
function addList(listName,boardId)
{
return fetch("https://api.trello.com/1/lists?name="+listName+"&idBoard="+boardId+"&key=7bcd9670004fd4a9b89efab01e92d773&token=14dc1d91d79d00ea6b7da57a9e85cb2ef8a63cfc1f7f2b23ec402416e71d2c70",{method:"post"})
.then((res)=>(res.json()))
.catch((error)=>console.log("hi"))
}
function addCard(cardName,cardId)
{
return fetch("https://api.trello.com/1/cards?idList="+cardId+"&keepFromSource=all&key=7bcd9670004fd4a9b89efab01e92d773&token=14dc1d91d79d00ea6b7da57a9e85cb2ef8a63cfc1f7f2b23ec402416e71d2c70&name="+cardName,{method:"post"})
.then((res)=>(res.json()))
.catch((error)=>console.log("hiiiiii"))
}
function getChecklists(cardId)
{
 return fetch("https://api.trello.com/1/cards/"+cardId+"/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=7bcd9670004fd4a9b89efab01e92d773&token=14dc1d91d79d00ea6b7da57a9e85cb2ef8a63cfc1f7f2b23ec402416e71d2c70")
.then((res)=>(res.json()))
.catch((error)=>console.log("hi"))
}
function addChecklist(checklistName,cardId)
{
    return fetch("https://api.trello.com/1/checklists?name="+checklistName+"&idCard="+cardId+"&key=7bcd9670004fd4a9b89efab01e92d773&token=14dc1d91d79d00ea6b7da57a9e85cb2ef8a63cfc1f7f2b23ec402416e71d2c70",{method:"post"})
    .then((res)=>(res.json()))
.catch((error)=>console.log("hi"))
}
function deleteChecklist(checklistId)
{
    return fetch("https://api.trello.com/1/checklists/"+checklistId+"?key=7bcd9670004fd4a9b89efab01e92d773&token=14dc1d91d79d00ea6b7da57a9e85cb2ef8a63cfc1f7f2b23ec402416e71d2c70",{method:"delete"})
}
module.exports={getBoards,getLists,getCards,addList,addCard,getChecklists,addChecklist,deleteChecklist};
